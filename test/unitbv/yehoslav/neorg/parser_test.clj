(ns unitbv.yehoslav.neorg.parser-test
  (:require [clojure.test :as test]
            [unitbv.yehoslav.neorg.lexer :as l]
            [unitbv.yehoslav.neorg.parser :as p]))

(test/deftest test-paragraph
  (test/testing "Parsing a paragraph"
    (test/is (= (-> "start:/italic/:end"
                    l/lex
                    p/paragraph
                    first)
                {:kind :paragraph,
                 :start {:kind :word, :literal "start", :position 0},
                 :content
                 [{:kind :word, :literal "start", :position 0}
                  {:kind :attached-modifier,
                   :type :italic,
                   :start {:kind :punctuation, :literal \/, :position 6},
                   :content [{:kind :word, :literal "italic", :position 7}]}
                  {:kind :word, :literal "end", :position 15}]}))

    (test/is (= (-> "/italic/"
                    l/lex
                    p/paragraph 
                    first) 
                {:kind :paragraph,
                 :start {:kind :punctuation, :literal \/, :position 0},
                 :content
                 [{:kind :attached-modifier,
                   :type :italic,
                   :start {:kind :punctuation, :literal \/, :position 0},
                   :content [{:kind :word, :literal "italic", :position 1}]}]})))
  (test/testing "Paragraph breaks"
    (test/is (= (-> "firstp\n\nsecondp"
                    l/lex
                    p/paragraph 
                    first) 
                {:kind :paragraph,
                 :start {:kind :word, :literal "firstp", :position 0},
                 :content [{:kind :word, :literal "firstp", :position 0}]}))))

;; TODO: Test all possible accepted and unaccepted attached-modifiers
(test/deftest test-attached-modifier
  (test/testing "Parsing an attached modifier"
    (test/is (= (-> "/italic/"
                    l/lex
                    p/attached-modifier
                    first) 
                {:kind :attached-modifier,
                   :type :italic,
                   :start {:kind :punctuation, :literal \/, :position 0},
                   :content [{:kind :word, :literal "italic", :position 1}]})))

  (test/testing "Parsing an attached-modifier that contains line breaks"
    (test/is (= (-> "/first\nsecond/"
                    l/lex
                    p/attached-modifier
                    first)
                {:kind :attached-modifier,
                 :type :italic,
                 :start {:kind :punctuation, :literal \/, :position 0},
                 :content [{:kind :word, :literal "first", :position 1}
                           {:kind :line-ending, :literal \newline, :position 6}
                           {:kind :word, :literal "second", :position 7}]})))

  (test/testing "Parsing link modifiers"
    (test/is (= (-> "_helo_:fren_"
                    l/lex
                    p/attached-modifier
                    first)
                {:kind :attached-modifier,
                 :type :underline,
                 :start {:kind :punctuation, :literal \_, :position 0},
                 :content
                 [{:kind :word, :literal "helo", :position 1}]})))

  (test/testing "Parsing free-form attached-modifier"
    (test/is (nil? (-> "/|italic\n\n|/"
                       l/lex
                       p/attached-modifier)))
    (test/is (= (-> "/|italic|/"
                    l/lex
                    p/attached-modifier
                    first)
                {:kind :attached-modifier,
                 :type :italic,
                 :start {:kind :punctuation, :literal \/, :position 0},
                 :content [{:kind :word, :literal "italic", :position 2}]})))

  (test/testing "Nested attached-modifier"
    (test/is (= (-> "/italic *bold/*"
                    l/lex
                    p/attached-modifier
                    first)
                   {:kind :attached-modifier,
                    :type :italic,
                    :start {:kind :punctuation, :literal \/, :position 0},
                    :content
                    [{:kind :word, :literal "italic", :position 1}
                     {:kind :white-space, :literal \space, :position 7}
                     {:kind :punctuation, :literal \*, :position 8}
                     {:kind :word, :literal "bold", :position 9}]}))
    (test/is (= (-> "/italic *bold*/"
                    l/lex
                    p/attached-modifier
                    first)
                {:kind :attached-modifier,
                 :type :italic,
                 :start {:kind :punctuation, :literal \/, :position 0},
                 :content
                 [{:kind :word, :literal "italic", :position 1}
                  {:kind :white-space, :literal \space, :position 7}
                  {:kind :attached-modifier,
                   :type :bold,
                   :start {:kind :punctuation, :literal \*, :position 8},
                   :content [{:kind :word, :literal "bold", :position 9}]}]})))

  (test/testing "Parsing a bad attached modifier"
    (test/is (nil? (-> "/ italic/"
                    l/lex
                    p/attached-modifier)))
    (test/is (nil? (-> "/italic /"
                    l/lex
                    p/attached-modifier)))))

(test/deftest test-linkables
  (test/testing "parse url links"
    (test/is (= (-> "{https://clojure.org}"
                    l/lex
                    p/linkable
                    first)
                {:kind :linkable,
                 :type :url,
                 :start {:kind :punctuation, :literal \u007b, :position 0},
                 :href "https://clojure.org",
                 :content []})))

  (test/testing "parse magic links"
    (test/is (= (-> "{# Magic}"
                    l/lex
                    p/linkable
                    first)
                 {:kind :linkable,
                  :type :magic,
                  :start {:kind :punctuation, :literal \u007b, :position 0},
                          :content
                          [{:kind :word, :literal "Magic", :position 3}]})))

  (test/testing "parse definition links"
    (test/is (= (-> "{$ Definition}"
                    l/lex
                    p/linkable
                    first)
                 {:kind :linkable,
                  :type :definition,
                  :start {:kind :punctuation, :literal \u007b, :position 0},
                          :content
                          [{:kind :word, :literal "Definition", :position 3}]})))

  (test/testing "parse footer links"
    (test/is (= (-> "{^ Footer}"
                    l/lex
                    p/linkable
                    first)
                 {:kind :linkable,
                  :type :footer,
                  :start {:kind :punctuation, :literal \u007b, :position 0},
                          :content
                          [{:kind :word, :literal "Footer", :position 3}]})))

  (test/testing "parse wiki links"
    (test/is (= (-> "{? Wiki}"
                    l/lex
                    p/linkable
                    first)
                {:kind :linkable,
                 :type :wiki,
                 :start {:kind :punctuation, :literal \u007b, :position 0},
                 :href "Wiki",
                 :content [{:kind :word, :literal "Wiki", :position 3}]})))

  (test/testing "parse links to neorg file"
    (test/is (= (-> "{:neorg_file:}"
                    l/lex
                    p/linkable
                    first)
                {:kind :linkable,
                 :type :neorg-file,
                 :start {:kind :punctuation, :literal \u007b, :position 0},
                 :href "neorg_file",
                 :target nil
                 :content
                 [{:kind :word, :literal "neorg", :position 2}
                  {:kind :punctuation, :literal \_, :position 7}
                  {:kind :word, :literal "file", :position 8}]})))
                 
  (test/testing "parse link header in a diffirent  neorg file"
    (test/is (= (-> "{:neorg_file:* Header}"
                    l/lex
                    p/linkable
                    first)
                {:kind :linkable,
                 :type :neorg-file,
                 :start {:kind :punctuation, :literal \u007b, :position 0},
                         :target
                         {:kind :linkable,
                          :type :header,
                          :level 1,
                          :start {:kind :punctuation, :literal \*, :position 13},
                          :content [{:kind :word, :literal "Header", :position 15}]},
                 :href "neorg_file",
                 :content
                 [{:kind :word, :literal "neorg", :position 2}
                  {:kind :punctuation, :literal \_, :position 7}
                  {:kind :word, :literal "file", :position 8}]}))))

(test/deftest test-bad-linkables

  (test/testing "missplaced braces"
    (test/is (nil? (-> "{\n* bad\n}" l/lex p/linkable)))
    (test/is (nil? (-> "{* bad\n}" l/lex p/linkable)))
    (test/is (nil? (-> "{\n* bad}" l/lex p/linkable))))

  (test/testing "missplaced modifiers"
    (test/is (nil? (-> "{*bad}" l/lex p/linkable)))
    (test/is (nil? (-> "{ * bad}" l/lex p/linkable)))
    (test/is (nil? (-> "{ :bad:}" l/lex p/linkable)))
    (test/is (nil? (-> "{:bad: }" l/lex p/linkable)))
    ;; TODO: check specification for: (test/is (nil? (-> "{: bad:}" l/lex p/linkable)))
    )

  (test/testing "Bad combinations of neorg-file link with detached modifiers"
    (test/is (nil? (-> "{:file:/ another.file}" l/lex p/linkable)))
    (test/is (nil? (-> "{:file:@ Monday 7th}" l/lex p/linkable)))
    (test/is (nil? (-> "{:file:https://inapropriate.com}" l/lex p/linkable)))))

(test/deftest test-structural-modifier
  (test/testing "parse first level heading" 
   (test/is (= (-> "* Head"
                 l/lex
                 (p/structural-modifier nil)
                 first)
                 {:kind :structural-modifier,
                   :heading
                   {:kind :heading,
                    :start {:kind :punctuation, :literal \*, :position 0},
                    :level 1,
                    :title
                    {:kind :paragraph-segment,
                     :start {:kind :word, :literal "Head", :position 2},
                     :content [{:kind :word, :literal "Head", :position 2}]}},
                   :content []})))
(test/testing "parse nested headings"
  (test/is (= (-> "* Head\n** Head 2"
                 l/lex
                 (p/structural-modifier nil)
                 first)
   {:kind :structural-modifier,
    :heading
    {:kind :heading,
     :start {:kind :punctuation, :literal \*, :position 0},
     :level 1,
     :title
     {:kind :paragraph-segment,
      :start {:kind :word, :literal "Head", :position 2},
      :content [{:kind :word, :literal "Head", :position 2}]}},
    :content
    [{:kind :structural-modifier,
      :heading
      {:kind :heading,
       :start {:kind :punctuation, :literal \*, :position 7},
       :level 2,
       :title
       {:kind :paragraph-segment,
        :start {:kind :word, :literal "Head", :position 10},
        :content
        [{:kind :word, :literal "Head", :position 10}
         {:kind :white-space, :literal \space, :position 14}
         {:kind :word, :literal "2", :position 15}]}},
      :content []}]})))

  (test/testing "parse bad structural-modifier modifier"
                 (test/is (nil? (-> "*Header"
                                    l/lex
                                    (p/structural-modifier nil))))
                 (test/is (nil? (-> "* \nHeader"
                                    l/lex
                                    (p/structural-modifier nil)))))
  )

