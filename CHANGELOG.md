# Change Log
All notable changes to this project will be documented in this file.

This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.1] Linkable tweaks

### Fixed
- Links to headings don't include the traling `{`

### Added
- Tests for bad linkables

### Changed
- Adapted tests to the new return shapes

## [1.1.0] Internal changes

### Changed
- Rewrote implementation of Linkable and Attached modifiers parsers.
- Now any url of form _protocol://_ will be parsed
- Updated all parsers to the now API

### Added 
- Implemented a naive support for all linkable types

### Fixed
- Check if EoF exists before parsing the document.

## [1.0.0] Changed API

### Changed
- All parsers will follow a common interface. Eeach parser will take as its first argument the list of tokens, and as its second argument a map of options. The parser will pull from the map the flags it needs, and pass the map to the parsers it calls updating it when necessary;
- Since all parsers are in the `parser` namespace they'll not be prefixed with `parse-`;
- Renamed the symbols to kebab-case to be consistent with clojure conventions.
### Fixed
- Nested attached modifiers should close in correct order, and up any amount of nesting levels.
