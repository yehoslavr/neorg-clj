(ns unitbv.yehoslav.neorg.parser
  (:require [unitbv.yehoslav.neorg.lexer :as l]))

;; INFO: The parsers should have a common interface so they can easily be passed
;; to combinator functions or macros
;; The proposed interface is (parsername token-list options)
;; where the options will be a map from which the parser will pull the needed options


(defn matching? [token1 token2]
  (and (= (:kind token1) (:kind token2))
       (= (:literal token1) (:literal token2))))

(defn match-token [[{:keys [literal kind] :as token} & rest_] {fliteral :literal fkind :kind}]
  (when-not (every? nil? [fkind fliteral])
    (when (matching? 
            {:literal (or fliteral literal)
             :kind    (or fkind    kind)}
            token)
      (list token rest_))))

(defn match-one-of [[{:keys [literal kind] :as token} & rest_] tokens-to-match]
  (loop [[{fliteral :literal fkind :kind} & rest-to-match] tokens-to-match]
    (when-not (every? nil? [fkind fliteral])
      (if (matching? 
            {:literal (or fliteral literal)
             :kind    (or fkind    kind)}
            token)
      (list token rest_)
      (recur rest-to-match)))))


(defn drop-match [value]
  (when-let [[_ rest-tokens] value] 
    [nil rest-tokens]))

(defn with-drop-match [parser] (comp drop-match parser))

(comment
  (-> " begin end"
      l/lex
      (match-one-of [{:kind :word :literal "end"} {:kind :word :literal "begin"}])))

(defn match-by-pred [[{ident_ :literal :as token} & rest_] {pred :pred}]
  (when-let [_ (pred ident_)]
    (list token rest_)))

(defn word 
  ([tokens] (word tokens nil))
  ([[{kind :kind :as token} & rest_] options]
    (when (= kind :word)
      (list token rest_))))

(def skip-whitespace 
  (partial drop-while (fn [{ident_ :literal}] (l/white-space? ident_))))

(defn escaped 
  ([tokens] (escaped tokens nil))
  ([[token & tokens] options]
   (when (matching? token (l/Punctuation \\))
     (when-let [[token & rest-tokens] tokens]
       (list {:kind :escaped :start token :token token } rest-tokens)))))

(defn line-end 
  ([tokens] (line-end tokens nil))
  ([tokens options]
   (when-let [[h t] (or (when-let [[_ rest] (match-token tokens {:literal \u000d})]
                         (when-let [res (match-token rest {:literal \u000a})] res))
                     (match-token tokens {:literal \u000a})
                     (match-token tokens {:literal \u000c}))]
     (list h (skip-whitespace t)))))

#_(defn words [tokens]
  (loop [[{kind :kind :as token} & rest_ :as tokens] tokens
         words []]
    (case kind
      :word (recur (skip-whitespace rest_) (conj words token))
      :line-ending (when-not (= :line-ending (first (first (skip-whitespace rest_))))
                     (recur (skip-whitespace rest_) words))
      (when words (list words tokens)))))

(defmacro por-> [tokens & parsers]
  "Passes the list of tokens to all the given functions as the first argument
   and executes all the fucntions inside an or statement"
  (let [funcs (map (fn [func]
                     (if (seq? func) 
                       (let [[pred & args] func]
                         (if (empty? args)
                           `(~pred ~tokens)
                           `(~pred ~tokens ~@args)))
                       `(~func ~tokens))) 
                   parsers)]
   `(or ~@funcs)))

(defn white-space 
  ([tokens] (white-space tokens nil)) 
  ([tokens options]
   (match-by-pred 
     tokens 
     (conj options  {:pred l/white-space?}))))

(defn punctiation
  ([tokens] (punctiation tokens nil)) 
  ([tokens options]
   (match-by-pred 
     tokens 
     (conj options {:pred l/punctuation?}))))

(declare attached-modifier
         linkable
         paragraph-segment)

(defn- inline [tokens options]
  (por-> tokens
         escaped
         word
         white-space
         linkable
         (attached-modifier options)
         punctiation
         line-end))

(defn match-till
  "@param tokens: the tokens to parse
  @param parsers: parsers that describe
  - how to :walk the token list, that is, how to interpret the tokens
  - condition to :find to consider the parsig successful
  - the :stop condition that when true exits the parser with nil."
  [tokens options]
  (if-let [findp (:find options)]
    (let [{stopp :stop
           walkp :walk
           :or {stopp (fn [_ _] nil)
                walkp inline}} options]

      (loop [tokens tokens
             last-kind (:last-kind options)
             parsed-tokens []]

        (when-not (empty? tokens) 
          (if-let [_ (stopp tokens {:last-kind last-kind})]
            nil
            (if-let [[token r] (findp tokens {:last-kind last-kind})]
              (list (if token (conj parsed-tokens token) parsed-tokens) r)
              (when-let [[token rest_] (walkp tokens {:last-kind last-kind})]
                (recur rest_ (:kind token) (conj parsed-tokens token))))))))
    (throw (AssertionError. (str "No find parser passed to match-till for token at position " (:position (first tokens)))))))

(defn ast-attached-modifier [start-token type content]
  {:kind :attached-modifier 
   :type type
   :start start-token
   :content content})

(defn verbatim [[token & rest-tokens] options]
  (list token rest-tokens))

(defn pand 
  "p[arse ]and will accumulate the tokens parsed by each passed parser unless any parser return nil
  in which case pand returns nil. Pand expects :parsers key in the options map, and it'll pass the options to each parser"
  [tokens {parsers :parsers :as options}]
  (if-not (nil? parsers)
    (loop [[parser & rest-parsers] parsers
           parsed-tokens []
           rest-tokens tokens]
      (if (nil? parser)
        (list parsed-tokens rest-tokens)
        (when-let [[token rest-tokens] (parser rest-tokens options)]
          (recur rest-parsers (conj parsed-tokens token) rest-tokens))))
    (throw (AssertionError. 
             (str 
               "pand: no parsers were given. Parsing token at position " 
               (:position (first tokens)))))))

(defn paragraph-break 
  ([tokens] (paragraph-break tokens nil))
  ([tokens options] 
   (when-let [[start-token rest_] (line-end tokens)]
     (when-let [[_ rest_] (line-end rest_)]
       (list {:kind :paragraph-break :start start-token} rest_)))))

(defn- url-location 
  [tokens options]
  (let [punct #(fn [ts _] (match-token ts (l/Punctuation %)))]
    (when-let [[words rest-tokens] (pand tokens
        (assoc options
               :parsers
               [word 
                (punct \:)
                (punct \/)
                (punct \/)]))]
      (when-let [[more-words rest-tokens] (match-till rest-tokens
                                                      {:find (fn [ts _]
                                                               (when-let [[_ rest_] (match-token ts (l/Punctuation \}))]
                                                                 [nil rest_]))
                                                       :walk verbatim
                                                       :stop (fn [t _] (por-> t
                                                                         (pand {:parsers 
                                                                                [line-end
                                                                                 (punct \})]})
                                                                         (paragraph-break t)))}
                                                      
                                                      )]
        (list {:kind :linkable 
               :type :url
               :href (apply str (map (fn [{:keys [kind literal]}] 
                                       (if (= :line-ending kind) \space literal))
                                     (concat words more-words)))
               :content []} rest-tokens)))))

(declare heading)

(defn neorg-path [tokens options]
 (when-let [[start-token rest-tokens] (match-token tokens (l/Punctuation \:))]
    (when-not (-> rest-tokens first :kind l/white-space?)
      (when-let [[path rest-tokens] (match-till rest-tokens
                                                (conj
                                                  {:find (with-drop-match
                                                           (fn [ts _] (match-token ts (l/Punctuation \:))))
                                                   :walk verbatim
                                                   :stop paragraph-break}
                                                  options))]
        (list {:kind :neorg-path
               :start start-token
               :href (apply str (map (fn [{:keys [kind literal]}] 
                                       (if (= :line-ending kind) \space literal))
                                     path))
               :content path}
              rest-tokens)))))

(defn line-number [tokens options]
  (when-let [[{literal :literal :as num} rest-tokens] (word tokens options)]
    (when (every? l/digit? literal)
      (when-let [[_ rest-tokens] (match-token rest-tokens (l/Punctuation \u007d))]
        (list {:kind :line-number
               :start num
               :line (Integer. literal)}
            rest-tokens)))))

(defn dettached-modifier [tokens {kind :kind-token max-level :max-level :as options}]
  (when (match-token tokens kind)
   (loop [num 0
          [token & rest-tokens] tokens]
        (cond
          (and (not (nil? max-level)) (> num max-level)) nil
          (matching? token kind) (recur (inc num) rest-tokens)
          (l/white-space? (:literal token)) (list num rest-tokens)
          :else nil))))


(defn- special-linkable 
  [tokens 
   {symbol :symbol
    type_ :type
    :as options}]
  (when-let [[level rest-tokens] (dettached-modifier
                                      tokens
                                      (conj {:max-level 1
                                       :kind-token (l/Punctuation symbol)} options))]
    (when-let [[segment rest-tokens] (paragraph-segment 
                                       rest-tokens 
                                       (assoc options 
                                              :find (fn [ts _] (drop-match (match-token ts (l/Punctuation \}))))
                                              :stop (fn [ts _]
                                                      (pand ts
                                                            {:parsers 
                                                             [line-end
                                                              (fn [ts _]
                                                                (match-token ts (l/Punctuation \})))]}))))]
      (list {:kind :linkable
             :type type_
             ; :level nil
             :start (first tokens)
             :content (:content segment)} rest-tokens))))

(defn- heading-link [tokens options]
 (when-let [[level rest-tokens] (dettached-modifier
                                      tokens
                                      (conj 
                                        {:max-level nil
                                         :kind-token (l/Punctuation \*)} options))]
    (when-let [[segment rest-tokens] (paragraph-segment 
                                       rest-tokens 
                                       (assoc options 
                                              :find (fn [ts _] (drop-match (match-token ts (l/Punctuation \}))))
                                              :stop (fn [ts _]
                                                      (pand ts
                                                            {:parsers 
                                                             [line-end
                                                              (fn [ts _]
                                                                (match-token ts (l/Punctuation \})))]}))))]
      (list {:kind :linkable
             :type :header
             :level level
             :start (first tokens)
             :content (:content segment)} rest-tokens))) 
  )

(defn- wiki-link [tokens options]
  (when-let [[link rest-tokens] (special-linkable
                                  tokens
                                  (assoc options
                                         :symbol \?
                                         :type :wiki))]
    (list (-> link
              (assoc :href (apply
                             str 
                             (map (fn [{:keys [kind literal]}] 
                                    (if (= :line-ending kind) \space literal))
                                  (get link :content))))
              (dissoc :level))  rest-tokens)))

(defn- extendable-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \= :type :extendable)))

(defn- footer-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \^ :type :footer)))

(defn- timestamp-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \@ :type :timestamp)))

(defn- definition-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \$ :type :definition)))

(defn- magic-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \# :type :magic)))

(defn- file-link [tokens options]
  (special-linkable  tokens (assoc options :symbol \/ :type :file)))

(defn- neorg-file-location [tokens options]
  (let [[path rest-tokens] (neorg-path tokens options)]
    (when-let [[head rest-tokens] (if (nil? rest-tokens)
                                    (line-number tokens options)
                                    (por-> rest-tokens
                                           (line-number options)
                                           (wiki-link options)
                                           (magic-link options)
                                           (footer-link options)
                                           (definition-link options)
                                           (heading-link options)
                                           ((fn [ts] (when (match-token ts (l/Punctuation \u007d))
                                                       [nil (next ts)]))) ))]
          (list {:kind :linkable
                 :type :neorg-file
                 :start (first tokens)
                 :target head
                 :href (:href path)
                 :content (:content path)} rest-tokens))))

(defn linkable
  ([tokens] (linkable tokens nil))
  ([tokens options]
   (when-let [[start-token rest-tokens] (match-token tokens (l/Punctuation \u007b))]
     (when-let [[link rest-tokens] (por-> rest-tokens
                                          (magic-link options)
                                          (url-location options)
                                          (heading-link options)
                                          (neorg-file-location options)
                                          (file-link options)
                                          (wiki-link options)
                                          (footer-link options)
                                          (definition-link options)
                                          (timestamp-link options)
                                          (extendable-link options)

                                          ,)]
       (list (assoc link :start start-token) rest-tokens)))))

(comment
  (-> "{* Hieader}[Description](extension) "
      l/lex
      (linkable {})))

(defn- attached-modifier- [tokens options]
  (when-let [[{start-ident :literal :as start-token} rest-tokens] (match-by-pred tokens {:pred l/attached-modifier?})]
    (when  (-> rest-tokens first :kind #{:word :punctuation})
      (when-let [[words rest-tokens] 
                 (match-till 
                   rest-tokens

                   {:find  (fn [[{literal :literal} {pkind :kind pliteral :literal} & rest_ :as tokens] options] 
                             (when (and (#{:word :attached-modifier :linkable :punctuation} (:last-kind options)) (= literal start-ident))
                               (cond 
                                 (and (= pliteral \:) (word rest_))  [nil rest_]
                                 (#{:eof :line-ending :punctuation :white-space} pkind) [nil (next tokens)])))

                    :walk (fn [tokens last-kind] 
                            (if (matching? start-token (l/Punctuation \`)) 
                              (verbatim tokens last-kind)
                              (inline tokens {:last-kind last-kind 
                                              :top-level (if-let [top-level (:top-level options)] 
                                                           (conj top-level start-ident) 
                                                           #{start-ident})})))
                    :stop (fn [tokens _] 
                            (por-> tokens
                                   ((fn [t]
                                      (when-let [top-level (:top-level options)] 
                                        (match-one-of t (map l/Punctuation top-level)))))
                                   (paragraph-break)))})]

      (list (ast-attached-modifier start-token (get l/attached-modifiers start-ident) words) 
            rest-tokens)))))

(defn- link-attached-modifier
  [tokens {last-kind :last-kind :as options}]
  (when (= :word last-kind)
    (when-let [[_ rest-tokens] (match-token tokens (l/Punctuation \:))]
      (when-let [[att-mod rest-tokens] (attached-modifier- rest-tokens (assoc options :last-kind :punctuation))]
        (list att-mod rest-tokens)))))

(defn- free-form-attached-modifier [tokens {last-kind :last-kind :as options}] 
  (when (or (nil? last-kind) (#{:word :attached-modifier :linkable :punctuation} last-kind))
  (when-let [[{start-ident :literal :as start-token} rest-tokens] (match-by-pred tokens {:pred l/attached-modifier?})]
  (when-let [[_ rest-tokens] (match-token rest-tokens (l/Punctuation \|))]
     (when-let [[words rest-tokens] 
                (match-till 
                  rest-tokens

                  {:find  (fn [[{literal :literal} {pliteral :literal} {pkind :kind } & _ :as tokens] _] 
                            (when (and 
                                    (= literal \|) 
                                    (= pliteral start-ident) 
                                    (#{:eof :line-ending :punctuation :white-space} pkind))
                              [nil (drop 2 tokens)]))

                   :walk (fn [ts o] (verbatim ts (conj options o)))
                   :stop (fn [tokens _] (paragraph-break tokens))})]

       (list (ast-attached-modifier start-token (get l/attached-modifiers start-ident) words) 
             rest-tokens))))))

(defn attached-modifier
  ([tokens] (attached-modifier tokens {}))
  ([tokens options]
   (por-> tokens
          (free-form-attached-modifier options)
          (attached-modifier- options)
          (link-attached-modifier options))))

(defn ast-paragraph [start-token content]
  {:kind :paragraph :start start-token :content content})

(defn eof 
  ([tokens] (eof tokens nil))
  ([tokens options]
  (when-let [[token & _] tokens]
    (when (= (:kind token) :eof)
     (list token tokens)))))

(defn- tag-name 
  ([tokens] (tag-name tokens nil))
  ([tokens options]
   (when-let [[start-token rest-tokens] (word tokens)] 
     (when-let [[symbols rest-tokens] (match-till rest-tokens
                                                 {:find (fn [tokens _]
                                                          (drop-match 
                                                            (por-> tokens
                                                                   white-space
                                                                   line-end)))
                                                  :walk (fn [tokens _]
                                                          (por-> tokens 
                                                                 word 
                                                                 (match-one-of (map l/Punctuation [\. \- \_]))))
                                                  :stop paragraph-break})]
       (list (cons start-token symbols) rest-tokens)))))

(defn end-tag [tokens {last-kind :last-kind}]
  (when (= last-kind :line-ending)
      (if-let [[_ rest-tokens] (white-space tokens)]
        (end-tag rest-tokens last-kind)
        (when-let [[start-token rest-tokens] (match-token tokens (l/Punctuation \@))]
          (when-let [[_ rest-tokens] (match-token rest-tokens (l/Word "end"))]
            (when-let [[_ rest-tokens] (por-> rest-tokens
                                              line-end
                                              eof )]
              (list start-token rest-tokens))

            ,)))))

(defn verbatim-ranged-tags [tokens {last-kind :last-kind}]
  (when (or (= last-kind :line-ending) (nil? last-kind))
      (if-let [[_ rest-tokens] (white-space tokens)]
        (verbatim-ranged-tags rest-tokens last-kind)
        (when-let [[start-token rest-tokens] (match-token tokens (l/Punctuation \@))]
        (when-let [[tag-name rest-tokens] (tag-name rest-tokens nil)]
        (when-let [[symbols rest-tokens] (match-till rest-tokens
                                                     {:find (with-drop-match end-tag)
                                                      :walk verbatim
                                                      :stop eof})]
          (list {:kind :ranged-tags
            :type :verbatim
            :start start-token
            :name tag-name
            :content symbols} rest-tokens))

            ,)))))


    
(defn paragraph-segment [tokens options]
  (when-not (line-end tokens)
    (when-let [[h t] (match-till 
                       tokens 
                       (conj 
                         {:find (fn [ts _] (drop-match (por-> ts line-end eof)))
                          :walk verbatim }
                         options))]
      (list {:kind :paragraph-segment :start (first tokens) :content h} t))))

(defn structural-modifier-level [tokens {last-kind :last-kind}]
  (when (or 
          (#{:line-ending :structural-modifier :paragraph :paragraph-segment :neorg-file} last-kind) 
          (nil? last-kind))
    (let [rest-tokens (skip-whitespace tokens)]
      (loop [num 0
             [token & rest-tokens] (if (= last-kind :neorg-file) tokens rest-tokens)]
        (cond
          (matching? token (l/Punctuation \*)) (recur (inc num) rest-tokens)
          (l/white-space? (:literal token)) (list num rest-tokens)
          :else nil)))))


(defn paragraph 
  ([tokens] (paragraph tokens nil))
  ([tokens options]
   (let [start-token (first tokens)]
     (when-let
       [[h t] (match-till 
                tokens 
                {:find  (fn [tokens options] 
                          (if (structural-modifier-level tokens options)
                            [nil tokens]
                            (drop-match (por-> tokens paragraph-break eof))))})]
       (list (ast-paragraph start-token h) t)))))

(defn heading [tokens options]
  (when-let [[level rest-tokens] (structural-modifier-level tokens options)]
    (when-let [[segment rest-tokens] (paragraph-segment rest-tokens options)]
      (list {:kind :heading
             :start (first tokens)
             :level level
             :title segment} rest-tokens))))

(defn delimiting-modifier [tokens {last-kind :last-kind}]
  (when (= last-kind :line-ending)
    (when-let [[start-token rest-tokens] (match-token tokens (l/Punctuation \-))]
     (when-let [[_ rest-tokens] (match-token rest-tokens (l/Punctuation \-))]
       (when-let [[_ rest-tokens] (line-end (drop-while (fn [{kind :kind}] (= kind :line-ending)) rest-tokens))]
         (list {:kind :delimiting-modifier
                :start start-token}
               rest-tokens))))))

(defn structural-modifier [tokens options]
  (when-let [[heading rest-tokens] (heading tokens options)]
    (when-let [[content rest-tokens] 
               (match-till
                 rest-tokens 
                 {:find (fn [tokens options] 
                          (drop-match (por-> tokens
                                                  ((fn [ts opts]
                                                     (when-let [[h _] (structural-modifier-level ts opts)]
                                                       (when (<= h (:level heading))
                                                         [nil ts]))) options)
                                                  (delimiting-modifier options)
                                                  eof)))
                  :walk (fn [tokens options] 
                          (por-> tokens
                                 (verbatim-ranged-tags options)
                                 (structural-modifier options)
                                 paragraph
                                 paragraph-break))})]
      (list {:kind :structural-modifier
             :heading heading
             :content content}
            rest-tokens))))

(defin document-metadata [tokens options]
  ([tokens] (document tokens nil))
  ([tokens options]

   )

  )

(defn document 
  ([tokens] (document tokens nil))
  ([tokens options]

   (assert (= :eof (-> tokens last :kind)) "End of File Expected")
   (when-let [[h t] (match-till tokens 
                                {:find (with-drop-match eof)
                                 :walk (fn [tokens options] 
                                         (por-> tokens
                                                (verbatim-ranged-tags options)
                                                (structural-modifier options)
                                                paragraph
                                                line-end
                                                white-space))
                                 :stop (fn [tokens _]  (assert (seq tokens)  "End of File Expected"))})]
     (list {:kind :document
            :start (first tokens)
            :content h}
           t))))

(comment
  (-> "/italic\nhehe/:end"
      l/lex
      attached-modifier
      first)

  (def tokens-istoric (->> "resources/content/istoric.norg"
       slurp
       l/lex
       document
       last


       #_norg->hiccup
       #_((fn [[tag & rest_]] [tag {:class "bg-yellow-50"} rest_]))))
  (first (document tokens-istoric {}))
  (-> "\n* Istoria "
      l/lex
      ; butlast
      document
      #_first)

  ;; FIXME: Escapes don't work. To make escapes work avoid using literals in the parser.
  (->> "`{? sir\\` Humpherey Davy}`"
      l/lex
      attached-modifier
      first
      :content
      (map :literal)
      (apply str))
      

  ,)
