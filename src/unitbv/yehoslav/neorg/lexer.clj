(ns unitbv.yehoslav.neorg.lexer)


(def white-space? #{\u0020 \u00A0})
(def line-endings? #{\u000a \u000c \u000d})
(def digit? #{\0 \1 \2 \3 \4 \5 \6 \7 \8 \9})
(def punctuation? #{ \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~})

(def dettached-modifiers {\* :heading \$ :definition \^ :footnote \: :table-cell})
(def dettached-modifier? (partial get dettached-modifiers))

(def linkable-dettached-modifiers {\# :magic \/ :file \@ :timestamp \? :wiki \= :extendable})
(def linkable-dettached-modifier? (partial get linkable-dettached-modifiers))

(def attached-modifiers {\* :bold \/ :italic \_ :underline \- :strike-through \! :spoiler \^ :superscript \, :subscript \` :inline-code \% :null-modifier \$ :inline-math \& :variable})
(def attached-modifier? (partial get attached-modifiers))

(def regular-character? (fn [ch] (not (or (punctuation? ch) (white-space? ch) (line-endings? ch)))))

(defn create [kind literal position]
  {:kind kind :literal literal :position position})

(defn Word
  ([literal] (Word literal 0))
  ([literal pos]
   (create :word literal pos)))

(defn Punctuation 
  ([literal] (Punctuation literal 0))
  ([literal pos]
   (create :punctuation literal pos)))


(defn to-str [chrs]
  (cond (seqable? chrs) (apply str chrs) 
        :else           (str chrs)))

;; TODO: Which is better? raw recursion like the one below or using a loop-recur
(defn lex
  ([input] (lex 0 input))
  ([pos [_char _peek & _rest :as input]]
   (cond
     (empty? input)        (list (create :eof nil pos))
     (white-space? _char)  (cons (create :white-space _char pos)
                                 (lex (inc pos) (drop-while white-space? input)))
     (line-endings? _char) (cons (create :line-ending _char pos)
                                 (lex (inc pos) (next input)))
     #_(= _char \\)          #_(cons (create :escaped _peek pos)
                                 (lex (+ 2 pos) _rest))
     (punctuation? _char)  (cons (Punctuation _char pos)
                                 (lex (inc pos) (next input)))
     :else (let [[word rst] (split-with regular-character? input)
                 npos (+ pos (count word))]
             (cons (Word (to-str word) pos)
                   (lex npos rst))))))


