(defproject neorg "1.1.1-SNAPSHOT"
  :description "Parser for Neorg: for didactic purposes"
  :url "https://gitlab.com/yehoslavr/neorg_parser"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :profiles {:repl {:plugins [[cider/cider-nrepl "0.47.0"]]}}
  :dependencies [[org.clojure/clojure "1.11.1"]])
