{pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-23.11.tar.gz") {}}:
with pkgs;
  mkShell {
    buildInputs = [
      clojure
      clojure-lsp
      clj-kondo
      leiningen
      # openjdk18-bootstrap
      pkgs.jdk21
    ];
  }
